<?php
declare(strict_types=1);

namespace App\Command;

use App\FakeUser\RendererInterface;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class RandomUserCommand extends Command
{
    protected static $defaultName = 'random-user';

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * RandomUserCommand constructor.
     * @param RendererInterface $renderer
     */
    public function __construct(RendererInterface $renderer)
    {
        parent::__construct();

        $this->renderer = $renderer;
    }

    protected function configure()
    {
        $this
            ->setDescription('Showing random users')
            ->addOption(
                'format-table',
                'f',
                InputOption::VALUE_NONE,
                'Should format users as table?'
            )
            ->addOption(
                'number',
                null,
                InputOption::VALUE_OPTIONAL,
                'How many users to generate?',
                null
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numberOfFakeUsers = null;
        if ($input->getOption('number') !== null) {
            if (!is_numeric($input->getOption('number')) || $input->getOption('number') < 0) {
                throw new InvalidArgumentException(sprintf("Given parameter %s is not valid", $input->getOption('number')));
            }
            $numberOfFakeUsers = (int)$input->getOption('number');
        }
        if ($input->getOption('format-table')) {
            $this->renderer->renderTable($output, $numberOfFakeUsers);
            return;
        }
        $this->renderer->renderJson($output, $numberOfFakeUsers);
    }
}