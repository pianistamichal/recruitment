<?php
declare(strict_types=1);

namespace App\FakeUser;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class Renderer implements RendererInterface
{

    public const FIRST_NAME_TITLE = "First Name";
    public const LAST_NAME_TITLE = "Last Name";
    public const ADDRESS_TITLE = "Address";

    /**
     * @var FakeUserGeneratorInterface
     */
    private $fakeUserGenerator;

    /**
     * Renderer constructor.
     * @param FakeUserGeneratorInterface $fakeUserGenerator
     */
    public function __construct(FakeUserGeneratorInterface $fakeUserGenerator)
    {
        $this->fakeUserGenerator = $fakeUserGenerator;
    }

    /**
     * @param OutputInterface $output
     * @param int|null $numberOfUsersToGenerate
     */
    public function renderJson(OutputInterface $output, ?int $numberOfUsersToGenerate): void
    {
        $elements = $this->fakeUserGenerator->generate($numberOfUsersToGenerate);

        $output->writeln(json_encode($elements));
    }

    /**
     * @param OutputInterface $output
     * @param int|null $numberOfUsersToGenerate
     */
    public function renderTable(OutputInterface $output, ?int $numberOfUsersToGenerate): void
    {
        $elements = $this->fakeUserGenerator->generate($numberOfUsersToGenerate);
        $parsedElements = [];
        foreach ($elements as $element) {
            $parsedElement = [
                $element->getFirstName(),
                $element->getLastName(),
                $element->getAddress()
            ];
            $parsedElements[] = $parsedElement;
        }
        $table = new Table($output);
        $table
            ->setHeaders([self::FIRST_NAME_TITLE, self::LAST_NAME_TITLE, self::ADDRESS_TITLE])
            ->setRows($parsedElements);
        $table->render();
    }
}