<?php
declare(strict_types=1);

namespace App\FakeUser;

use JsonSerializable;

class FakeUserModel implements JsonSerializable
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $streetName;

    /**
     * @var string
     */
    private $buildingNumber;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $postcode;

    /**
     * FakeUserModel constructor.
     * @param string $firstName
     * @param string $lastName
     * @param string $streetName
     * @param string $buildingNumber
     * @param string $city
     * @param string $postcode
     */
    public function __construct(string $firstName, string $lastName, string $streetName, string $buildingNumber, string $city, string $postcode)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->streetName = $streetName;
        $this->buildingNumber = $buildingNumber;
        $this->city = $city;
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return "{$this->streetName}, {$this->buildingNumber}, {$this->city}, {$this->postcode}";
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'address' => [
                'street' => $this->getStreetName(),
                'number' => $this->getBuildingNumber(),
                'city' => $this->getCity(),
                'postCode' => $this->getPostcode()
            ]
        ];
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getStreetName(): string
    {
        return $this->streetName;
    }

    /**
     * @return string
     */
    public function getBuildingNumber(): string
    {
        return $this->buildingNumber;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }
}