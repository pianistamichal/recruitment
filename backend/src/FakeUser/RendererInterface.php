<?php
declare(strict_types=1);

namespace App\FakeUser;


use Symfony\Component\Console\Output\OutputInterface;

interface RendererInterface
{
    /**
     * @param OutputInterface $output
     * @param int|null $numberOfUsersToGenerate
     */
    public function renderJson(OutputInterface $output, ?int $numberOfUsersToGenerate): void;

    /**
     * @param OutputInterface $output
     * @param int|null $numberOfUsersToGenerate
     */
    public function renderTable(OutputInterface $output, ?int $numberOfUsersToGenerate): void;
}