<?php
declare(strict_types=1);

namespace App\FakeUser;


use Faker\Generator;

class FakeUserGenerator implements FakeUserGeneratorInterface
{
    /**
     * @var Generator
     */
    private $generator;

    /**
     * Renderer constructor.
     * @param Generator $generator
     */
    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    /**
     * @param int $amountOfFakeUsers
     * @return array
     */
    public function generate(?int $amountOfFakeUsers = null): array
    {
        if ($amountOfFakeUsers === null) {
            $amountOfFakeUsers = self::DEFAULT_AMOUNT_OF_FAKE_USERS;
        }
        $users = [];
        for ($i = 0; $i < $amountOfFakeUsers; $i++) {
            $users[] = $this->generateOne();
        }

        return $users;
    }

    /**
     * @return FakeUserModel
     */
    public function generateOne(): FakeUserModel
    {
        return new FakeUserModel(
            $this->generator->firstName,
            $this->generator->lastName,
            $this->generator->streetName,
            $this->generator->buildingNumber,
            $this->generator->city,
            $this->generator->postcode
        );
    }
}