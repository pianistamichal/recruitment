<?php
declare(strict_types=1);

namespace App\FakeUser;


interface FakeUserGeneratorInterface
{
    const DEFAULT_AMOUNT_OF_FAKE_USERS = 20;

    /**
     * @param int|null $amountOfFakeUsers
     * @return FakeUserModel[]
     */
    public function generate(?int $amountOfFakeUsers = null): array;

    /**
     * @return FakeUserModel
     */
    public function generateOne(): FakeUserModel;
}