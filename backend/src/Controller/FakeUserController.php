<?php
declare(strict_types=1);

namespace App\Controller;

use App\FakeUser\FakeUserGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FakeUserController
{
    /**
     * @var FakeUserGeneratorInterface
     */
    private $fakeUserGenerator;

    /**
     * FakeUserController constructor.
     * @param FakeUserGeneratorInterface $fakeUserGenerator
     */
    public function __construct(FakeUserGeneratorInterface $fakeUserGenerator)
    {
        $this->fakeUserGenerator = $fakeUserGenerator;
    }

    /**
     * @return Response
     */
    public function indexAction(): Response
    {
        $users = $this->fakeUserGenerator->generate();
        return new JsonResponse($users, Response::HTTP_OK);
    }
}