<?php
declare(strict_types=1);

namespace App;

use Faker\Factory;
use Faker\Generator;

class FakerGeneratorFactory
{
    /**
     * @return Generator
     */
    public static function createFakerGenerator(): Generator
    {
        return Factory::create();
    }
}