$(document).ready(function() {
    let dataTable = $('#fakeUserTable').DataTable();
    fetch("http://127.0.0.1:8000")
        .then(resp => {
            return resp.json();
        }).then(function(json) {
            json.forEach(function (test) {
                dataTable.row.add([
                    test['firstName'],
                    test['lastName'],
                    test['address']['street'],
                    test['address']['number'],
                    test['address']['city'],
                    test['address']['postCode'],
                ]).draw(false);
            });
        });
});