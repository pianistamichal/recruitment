#Rekrutacja - Michał Błociński

##Zad 1
###Treść

Znajdz błąd w zapytaniu: ( PostreSQL )
```mysql
SELECT p.id, p.number, SUM(i.premium)
FROM policy p
RIGHT JOIN installment i ON i.policy_id = p.id
HAVING COUNT(i.id) > 1
```

Napisza zapytania tworzące table do zapytania powyżej, które będą wydajne przy dużej licznie danych.
###Rozwiązania:
####Zapytanie powinno wyglądać tak:
```mysql
SELECT p.id, p.number, SUM(i.premium)
FROM policy p
RIGHT JOIN installment i ON i.policy_id = p.id
GROUP BY p.id
HAVING COUNT(p.id) > 1
```

####Zapytania tworzące:
```mysql
create table installment
(
	id int auto_increment primary key,
	policy_id int null,
	premium float null,
	constraint installment_policy_id_fk
		foreign key (policy_id) references policy (id)
);
```

```mysql
create table policy
(
	id int auto_increment primary key,
	number int null
);
```
##Zad 2:
###Treść
Napisz prostą aplikację konsolową w jęzuku PHP której wynikiem będzie lista użytkowników zawierająca ("First Name", "Last Name", "Address") w formacie JSON oraz w formie wizualnej. 

np:.


php bin/console random-user -f               
```
+------------+-------------+--------------------------------------------------------+
| First Name | Last Name   | Address                                                |
+------------+-------------+--------------------------------------------------------+
| Madge      | Nikolaus    | Faye Highway,3,Katrineberg,67916-5937                  |
| Bettye     | Berge       | Koch Hill,85,Reannachester,83104-6567                  |
| Fern       | Heller      | Jacobs Place,9,Erdmanshire,76396-9146                  |
| Bella      | Heathcote   | Walsh Forge,25548403,South Vince,04535                 |
| Nelda      | Thiel       | Pink Freeway,8,East Reannachester,53282-4210           |
| Magnus     | Russel      | Flatley Falls,799022926,Lake Hiltonton,78643           |
| Eve        | Koelpin     | Smith Haven,279624,Lorenzaview,34215                   |
| Anahi      | Leffler     | Balistreri Tunnel,12403346,East Chesterbury,76120-6606 |
| Ayana      | Blanda      | Pfannerstill Circle,1589681,Martinmouth,93999          |
| Maxie      | Oberbrunner | Klein Wells,970,New Joy,57731-8891                     |
| Lance      | Bayer       | Arvid View,584,Swiftfort,74937-8054                    |
| Ulices     | Hills       | Garfield Court,47905,Lake Brandi,77592-8044            |
| Myrna      | Funk        | Gutkowski Lodge,11,Weststad,70610-3433                 |
+------------+-------------+--------------------------------------------------------+

```                                                                                                              
php bin/console random-user
```
[{"firstName":"Danyka","lastName":"Raynor","address":{"street":"Runte Glens","number":1,"city":"Lake Elenorshire","postalCode":"36487"}}]
```

Wykorzystaj w tym celu composera, symfony console oraz bibliotekę do generowania przykładowych danych: https://github.com/fzaninotto/Faker

###Rozwiązanie(opis)
Katalog backend. Run `composer install`. Potem `bin/console random-user -f` lub `bin/console random-user`. Aby zdefiniować ilość generowanych fake userów dodaj flagę --number=ilosc np `bin/console random-user -f --number=20`. Domyślna generowana ilość to 20 userów

##Zad 3:
###Treść
Stwórz widok HTML ( tabela ) którą będzie można posortować po kolumnach, dane do tabeli powinny być pobierane przez GET ( wykorzystaj kod z zadania 2 )

###Rozwiązanie(opis)
Podepnij front pod serwer www(np.apache). Api backend odpal komendą: `php bin/console server:start`.
